$isRunning=1
$index=0

while ( $isRunning -eq 1 )
{
	Write-Host "[1] capture packets"
	Write-Host "[2] read packets"

	$option = Read-Host
	if ( $option -eq 1 )
    {
        Write-Host "eth0 for vmware kali og WiFi for windows bærbar"
        $interface =  Read-Host "enter interface: "
        
        $capturefile = Read-Host "enter name of capture file: "
        
        Write-Host "CTRL-C to stop capturing"
        C:\'Program Files'\Wireshark\tshark.exe -i $interface -w "$capturefile.pcapng"
    }  
    elseif ( $option -eq 2 )
    {
        $files=(
        Get-ChildItem . |
        Where-Object -Property Name -Like *.pcapng |
        ForEach-Object {$_.Name})
        
        for($i = 0; $i -lt $files.Length; $i++)
        {
            $index=$i+1
            Write-Host [$index] $files[$i]
        }
        $chosenFile = Read-Host "choose file"
        $chosenFile = [int]$chosenFile
        $chosenFile--
        C:\'Program Files'\Wireshark\tshark.exe -r $files[$chosenFile]
    }
    else
    {
        $isRunning=0
    }
}
exit 0
