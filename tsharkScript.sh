#!/bin/bash

isRunning=true
files=()

while [ $isRunning ]
do
	incrementer=1
	echo "[1] capture packets"
	echo "[2] read packets"

	read option
	if [[ $option == 1 ]] then
		
		echo "enter interface: "
		read interface
		
		echo "enter name of capture file: "
		read capturefile
		
		echo "CTRL-C to stop capturing"
		tshark -i $interface -w "$capturefile.pcapng"
	
	elif [[ $option == 2 ]] then
		echo "choose file"

		for file in $(find *.pcapng)
		do
			echo [$incrementer]$file
			files+=($file)
			((incrementer+=1))
		done
		
		read chosenFile
		echo "tshark -r ${files[$chosenFile-1]}"
	else
		isRunning=false
	fi

done	

exit 0
